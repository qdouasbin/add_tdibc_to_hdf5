"""
This script reads a hdf5 file and adds a dataset "Tdibc" containing the poles and residues
"""

import h5py
import numpy as np
import params as prm


# ---------------------------
# Function(s)


def print_attrs(name, obj):
    "Print names and attributes of an object"
    print "\t" + name
    for key, val in obj.attrs.iteritems():
        print "%s: %s" % (key, val)


# ---------------------------


# Read input file
H5F_IN = h5py.File(prm.HDF_FILE_IN, 'r')

# Info on input file
print "\nHDF5 input file as the following structure:\n"
H5F_IN.visititems(print_attrs)

# Create output file
H5F_OUT = h5py.File(prm.HDF_FILE_OUT, 'w')

# Copy input file onto output file
for k in H5F_IN.keys():
    print "key = %s" % k
    # Create group
    H5F_OUT.create_group(k)
    for i in H5F_IN[k].keys():
        H5F_IN.copy(k + '/' + i, H5F_OUT[k])

print "\nCopied HDF5 file as the following structure:\n"
H5F_OUT.visititems(print_attrs)
print "\n\n\n"

print "Loading data in file: %s" % prm.TDIBC_ASCII_FILE
ASCII_DATA = np.loadtxt(prm.TDIBC_ASCII_FILE, skiprows=0)

# Recover first column
N_OSC = ASCII_DATA[0]
print "N_OSC = %s" % N_OSC

# Creating output variables
RE_POLES, IM_POLES, RE_RESIDUES, IM_RESIDUES = [], [], [], []
DATASET_NAMES = ['Re_Poles', 'Im_Poles', 'Re_Residues', 'Im_Residues']
DATASET_VALUES = [RE_POLES, IM_POLES, RE_RESIDUES, IM_RESIDUES]

# Fill list for Datasets
for i in xrange(int(N_OSC)):
    count = 1
    for list_var in [RE_POLES, IM_POLES, RE_RESIDUES, IM_RESIDUES]:
        print list_var
        print "Reading dataset %s " % (4 * i + count)
        tmp = ASCII_DATA[4 * i + count]
        print "Which is: %f " % tmp
        list_var.append(tmp)
        count += 1

LIST_NB_POLES = [N_OSC] * len(RE_POLES)
DATASET_NAMES.append('nb_poles')
DATASET_VALUES.append(LIST_NB_POLES)

# Add Datasets
for i, dt_nm in enumerate(DATASET_NAMES):
    print "dataset %s, datavalue -->" % DATASET_NAMES[i], DATASET_VALUES[i]
    H5F_OUT.create_dataset("/" + prm.GROUP_NAME + "/%s" %
                           dt_nm, data=DATASET_VALUES[i])

# Info on output file
print "\nHDF5 ouput file as the following structure:\n"
H5F_OUT.visititems(print_attrs)
