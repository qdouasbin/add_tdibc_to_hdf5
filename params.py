"""
This is the input file of AddTdibcToHDF
"""

# Ascii file (output of the preprocessing)
TDIBC_ASCII_FILE = './02_poles_model.ascii'

# Input HDF5 file
HDF_FILE_IN = './IBC_1D_1001.solutBound.h5'

# Output HDF5 file
HDF_FILE_OUT = './IBC_1D_1001_withTDIBC.solutBound.h5'

# Group name to be added
GROUP_NAME = 'Patch_002-rightWall'
